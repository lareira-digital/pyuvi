ARG ALPINE_VERSION=latest
FROM alpine:$ALPINE_VERSION

ARG LAREIRA_POETRY_VERSION

ENV LAREIRA_DOCKER_USER="lardock" \
    LAREIRA_DOCKER_GROUP="largroup" \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=${LAREIRA_POETRY_VERSION} \
    POETRY_VIRTUALENVS_IN_PROJECT=true
ENV LAREIRA_HOME="/home/$LAREIRA_DOCKER_USER"
ENV PATH="$LAREIRA_HOME/.venv/bin:$LAREIRA_HOME/.local/bin:$PATH"

# Create base user and group
RUN addgroup -g 1001 $LAREIRA_DOCKER_GROUP && \
    adduser -G $LAREIRA_DOCKER_GROUP --disabled-password -s /bin/sh -g "$LAREIRA_DOCKER_USER" $LAREIRA_DOCKER_USER

# Install all the systems depencencies that are required for the rest of the steps
RUN apk add --no-cache git curl python3 python3-dev py3-pip

# From here on out, everything is resposibility os LAREIRA_DOCKER_USER
USER $LAREIRA_DOCKER_USER
RUN curl -sSL https://install.python-poetry.org | POETRY_VERSION=$LAREIRA_POETRY_VERSION python3 - 
